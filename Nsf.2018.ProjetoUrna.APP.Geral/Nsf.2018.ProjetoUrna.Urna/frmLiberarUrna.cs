﻿using Nsf.ProjetoUrna.APP.ELEITOR;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf._2018.ProjetoUrna.Urna
{
    public partial class frmLiberarUrna : Form
    {
        public frmLiberarUrna()
        {
            InitializeComponent();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 1)
            {
                DTO_Permissao linha = dgvUrna.CurrentRow.DataBoundItem as DTO_Permissao;
                int id = linha.ID;
                bool acesso = linha.Situacao;

                acesso = true;

                Business_Permissao db = new Business_Permissao();
                db.Alterar(id, acesso);
            }
            if (e.ColumnIndex == 2)
            {
                DTO_Permissao linha = dgvUrna.CurrentRow.DataBoundItem as DTO_Permissao;
                int id = linha.ID;
                bool acesso = linha.Situacao;
                acesso = false;

                Business_Permissao db = new Business_Permissao();
                db.Alterar(id, acesso);
            }
        }
    }
}
