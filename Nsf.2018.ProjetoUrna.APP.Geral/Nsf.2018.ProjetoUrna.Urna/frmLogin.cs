﻿using Nsf.ProjetoUrna.APP.ELEITOR;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf._2018.ProjetoUrna.Urna
{
    public partial class frmLogin : Form
    {


        public frmLogin()
        {
            InitializeComponent();
            lblid.Text = pa.ID.ToString();
        }

        DTO_Permissao pa = new DTO_Permissao();

        private void button1_Click(object sender, EventArgs e)
        {
            PermitirAcesso();

        }
        public void PermitirAcesso()
        {
            //dto 

            pa.ID = int.Parse(lblid.Text);

            //business
            Business_Permissao bpa = new Business_Permissao();
            bool acesso = bpa.ConsultarPermissao(pa);

            //Pode acessar
            if (acesso == true)
            {
                frmUrna urna = new frmUrna();
                this.Close();
                urna.Show();
            }
            else
            {

            }

        }
    }
}
