﻿namespace Nsf._2018.ProjetoUrna.Urna
{
    partial class frmUrna
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlVoto = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblVice = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lvlCandidatura = new System.Windows.Forms.Label();
            this.lblNumero = new System.Windows.Forms.Label();
            this.lblPartido = new System.Windows.Forms.Label();
            this.lblNome = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.dgvCandidato = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnConfirmar = new System.Windows.Forms.Button();
            this.pnlPainel = new System.Windows.Forms.Panel();
            this.btnBranco = new System.Windows.Forms.Button();
            this.btnCorrigir = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.pnlVoto.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCandidato)).BeginInit();
            this.pnlPainel.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlVoto
            // 
            this.pnlVoto.BackColor = System.Drawing.Color.White;
            this.pnlVoto.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlVoto.Controls.Add(this.label4);
            this.pnlVoto.Controls.Add(this.label3);
            this.pnlVoto.Controls.Add(this.label2);
            this.pnlVoto.Controls.Add(this.groupBox2);
            this.pnlVoto.Controls.Add(this.groupBox1);
            this.pnlVoto.Controls.Add(this.lblNumero);
            this.pnlVoto.Controls.Add(this.lblPartido);
            this.pnlVoto.Controls.Add(this.lblNome);
            this.pnlVoto.Controls.Add(this.pictureBox1);
            this.pnlVoto.Location = new System.Drawing.Point(13, 57);
            this.pnlVoto.Margin = new System.Windows.Forms.Padding(2);
            this.pnlVoto.Name = "pnlVoto";
            this.pnlVoto.Size = new System.Drawing.Size(555, 339);
            this.pnlVoto.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Consolas", 20F);
            this.label4.Location = new System.Drawing.Point(156, 185);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(225, 32);
            this.label4.TabIndex = 10;
            this.label4.Text = "______________";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Consolas", 20F);
            this.label3.Location = new System.Drawing.Point(140, 76);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(240, 32);
            this.label3.TabIndex = 9;
            this.label3.Text = "_______________";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Consolas", 20F);
            this.label2.Location = new System.Drawing.Point(112, 138);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(270, 32);
            this.label2.TabIndex = 8;
            this.label2.Text = "_________________";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.lblVice);
            this.groupBox2.Location = new System.Drawing.Point(0, 259);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(555, 81);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Consolas", 20F);
            this.label1.Location = new System.Drawing.Point(249, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(285, 32);
            this.label1.TabIndex = 5;
            this.label1.Text = "__________________";
            // 
            // lblVice
            // 
            this.lblVice.AutoSize = true;
            this.lblVice.Font = new System.Drawing.Font("Consolas", 20F);
            this.lblVice.Location = new System.Drawing.Point(22, 26);
            this.lblVice.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblVice.Name = "lblVice";
            this.lblVice.Size = new System.Drawing.Size(240, 32);
            this.lblVice.TabIndex = 4;
            this.lblVice.Text = "Vice candidato:";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.White;
            this.groupBox1.Controls.Add(this.lvlCandidatura);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.groupBox1.Location = new System.Drawing.Point(132, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(301, 50);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            // 
            // lvlCandidatura
            // 
            this.lvlCandidatura.AutoSize = true;
            this.lvlCandidatura.Font = new System.Drawing.Font("Consolas", 20F);
            this.lvlCandidatura.Location = new System.Drawing.Point(108, 9);
            this.lvlCandidatura.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lvlCandidatura.Name = "lvlCandidatura";
            this.lvlCandidatura.Size = new System.Drawing.Size(90, 32);
            this.lvlCandidatura.TabIndex = 0;
            this.lvlCandidatura.Text = "Cargo";
            // 
            // lblNumero
            // 
            this.lblNumero.AutoSize = true;
            this.lblNumero.Font = new System.Drawing.Font("Consolas", 20F);
            this.lblNumero.Location = new System.Drawing.Point(34, 76);
            this.lblNumero.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblNumero.Name = "lblNumero";
            this.lblNumero.Size = new System.Drawing.Size(120, 32);
            this.lblNumero.TabIndex = 5;
            this.lblNumero.Text = "NÚMERO:";
            // 
            // lblPartido
            // 
            this.lblPartido.AutoSize = true;
            this.lblPartido.Font = new System.Drawing.Font("Consolas", 20F);
            this.lblPartido.Location = new System.Drawing.Point(34, 185);
            this.lblPartido.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPartido.Name = "lblPartido";
            this.lblPartido.Size = new System.Drawing.Size(135, 32);
            this.lblPartido.TabIndex = 3;
            this.lblPartido.Text = "Partido:";
            // 
            // lblNome
            // 
            this.lblNome.AutoSize = true;
            this.lblNome.Font = new System.Drawing.Font("Consolas", 20F);
            this.lblNome.Location = new System.Drawing.Point(34, 138);
            this.lblNome.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblNome.Name = "lblNome";
            this.lblNome.Size = new System.Drawing.Size(90, 32);
            this.lblNome.TabIndex = 2;
            this.lblNome.Text = "Nome:";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(395, 76);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(114, 141);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // dgvCandidato
            // 
            this.dgvCandidato.AllowUserToAddRows = false;
            this.dgvCandidato.AllowUserToDeleteRows = false;
            this.dgvCandidato.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvCandidato.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(70)))), ((int)(((byte)(178)))));
            this.dgvCandidato.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvCandidato.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCandidato.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4});
            this.dgvCandidato.GridColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.dgvCandidato.Location = new System.Drawing.Point(580, 57);
            this.dgvCandidato.Margin = new System.Windows.Forms.Padding(2);
            this.dgvCandidato.Name = "dgvCandidato";
            this.dgvCandidato.ReadOnly = true;
            this.dgvCandidato.RowTemplate.Height = 24;
            this.dgvCandidato.Size = new System.Drawing.Size(280, 181);
            this.dgvCandidato.TabIndex = 5;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "ID";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Nome";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Número";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Partido";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            // 
            // btnConfirmar
            // 
            this.btnConfirmar.BackColor = System.Drawing.Color.DodgerBlue;
            this.btnConfirmar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnConfirmar.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConfirmar.Location = new System.Drawing.Point(14, 90);
            this.btnConfirmar.Margin = new System.Windows.Forms.Padding(2);
            this.btnConfirmar.Name = "btnConfirmar";
            this.btnConfirmar.Size = new System.Drawing.Size(255, 52);
            this.btnConfirmar.TabIndex = 2;
            this.btnConfirmar.Text = "Confirma";
            this.btnConfirmar.UseVisualStyleBackColor = false;
            this.btnConfirmar.Click += new System.EventHandler(this.btnConfirmar_Click);
            // 
            // pnlPainel
            // 
            this.pnlPainel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(70)))), ((int)(((byte)(178)))));
            this.pnlPainel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlPainel.Controls.Add(this.btnBranco);
            this.pnlPainel.Controls.Add(this.btnCorrigir);
            this.pnlPainel.Controls.Add(this.btnConfirmar);
            this.pnlPainel.Location = new System.Drawing.Point(580, 237);
            this.pnlPainel.Margin = new System.Windows.Forms.Padding(2);
            this.pnlPainel.Name = "pnlPainel";
            this.pnlPainel.Size = new System.Drawing.Size(280, 159);
            this.pnlPainel.TabIndex = 6;
            // 
            // btnBranco
            // 
            this.btnBranco.BackColor = System.Drawing.Color.White;
            this.btnBranco.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnBranco.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Bold);
            this.btnBranco.Location = new System.Drawing.Point(14, 18);
            this.btnBranco.Margin = new System.Windows.Forms.Padding(2);
            this.btnBranco.Name = "btnBranco";
            this.btnBranco.Size = new System.Drawing.Size(123, 52);
            this.btnBranco.TabIndex = 4;
            this.btnBranco.Text = "Branco";
            this.btnBranco.UseVisualStyleBackColor = false;
            this.btnBranco.Click += new System.EventHandler(this.btnBranco_Click);
            // 
            // btnCorrigir
            // 
            this.btnCorrigir.BackColor = System.Drawing.Color.Red;
            this.btnCorrigir.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCorrigir.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Bold);
            this.btnCorrigir.Location = new System.Drawing.Point(151, 18);
            this.btnCorrigir.Margin = new System.Windows.Forms.Padding(2);
            this.btnCorrigir.Name = "btnCorrigir";
            this.btnCorrigir.Size = new System.Drawing.Size(118, 52);
            this.btnCorrigir.TabIndex = 3;
            this.btnCorrigir.Text = "Corrigir";
            this.btnCorrigir.UseVisualStyleBackColor = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(21)))), ((int)(((byte)(1)))));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Location = new System.Drawing.Point(-4, -9);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(883, 53);
            this.panel1.TabIndex = 7;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(21)))), ((int)(((byte)(1)))));
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.label5);
            this.panel2.Location = new System.Drawing.Point(219, 6);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(443, 38);
            this.panel2.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(21)))), ((int)(((byte)(1)))));
            this.label5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label5.Font = new System.Drawing.Font("Consolas", 20F);
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(54, 1);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(330, 32);
            this.label5.TabIndex = 9;
            this.label5.Text = " URNA ELETORAL INFO B";
            // 
            // frmUrna
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(874, 411);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pnlPainel);
            this.Controls.Add(this.pnlVoto);
            this.Controls.Add(this.dgvCandidato);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmUrna";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.pnlVoto.ResumeLayout(false);
            this.pnlVoto.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCandidato)).EndInit();
            this.pnlPainel.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlVoto;
        private System.Windows.Forms.Label lblVice;
        private System.Windows.Forms.DataGridView dgvCandidato;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.Button btnConfirmar;
        private System.Windows.Forms.Panel pnlPainel;
        private System.Windows.Forms.Button btnBranco;
        private System.Windows.Forms.Button btnCorrigir;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lvlCandidatura;
        private System.Windows.Forms.Label lblNumero;
        private System.Windows.Forms.Label lblPartido;
        private System.Windows.Forms.Label lblNome;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label5;
    }
}

