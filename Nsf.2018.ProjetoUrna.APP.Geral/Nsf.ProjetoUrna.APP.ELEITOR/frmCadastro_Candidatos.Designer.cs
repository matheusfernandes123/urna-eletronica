﻿namespace Nsf.ProjetoUrna.APP.ELEITOR
{
    partial class frmCadastro_Candidatos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.lblNumero = new System.Windows.Forms.Label();
            this.lblPartido = new System.Windows.Forms.Label();
            this.lblNome = new System.Windows.Forms.Label();
            this.lblVice = new System.Windows.Forms.Label();
            this.txtCandidato = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.imgCandidato = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.txtVice = new System.Windows.Forms.TextBox();
            this.nudNumero = new System.Windows.Forms.NumericUpDown();
            this.btnCadastrar = new System.Windows.Forms.Button();
            this.cboPartido = new System.Windows.Forms.ComboBox();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgCandidato)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudNumero)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(21)))), ((int)(((byte)(1)))));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Location = new System.Drawing.Point(0, -1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(708, 53);
            this.panel1.TabIndex = 8;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(21)))), ((int)(((byte)(1)))));
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.label5);
            this.panel2.Location = new System.Drawing.Point(129, 10);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(443, 38);
            this.panel2.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(21)))), ((int)(((byte)(1)))));
            this.label5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label5.Font = new System.Drawing.Font("Consolas", 20F);
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(54, 1);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(345, 32);
            this.label5.TabIndex = 9;
            this.label5.Text = "Cadastro de Candidatos";
            // 
            // lblNumero
            // 
            this.lblNumero.AutoSize = true;
            this.lblNumero.Font = new System.Drawing.Font("Consolas", 10F);
            this.lblNumero.Location = new System.Drawing.Point(73, 70);
            this.lblNumero.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblNumero.Name = "lblNumero";
            this.lblNumero.Size = new System.Drawing.Size(64, 17);
            this.lblNumero.TabIndex = 14;
            this.lblNumero.Text = "NÚMERO:";
            // 
            // lblPartido
            // 
            this.lblPartido.AutoSize = true;
            this.lblPartido.Font = new System.Drawing.Font("Consolas", 10F);
            this.lblPartido.Location = new System.Drawing.Point(65, 107);
            this.lblPartido.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPartido.Name = "lblPartido";
            this.lblPartido.Size = new System.Drawing.Size(72, 17);
            this.lblPartido.TabIndex = 12;
            this.lblPartido.Text = "Partido:";
            // 
            // lblNome
            // 
            this.lblNome.AutoSize = true;
            this.lblNome.Font = new System.Drawing.Font("Consolas", 10F);
            this.lblNome.Location = new System.Drawing.Point(89, 34);
            this.lblNome.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblNome.Name = "lblNome";
            this.lblNome.Size = new System.Drawing.Size(48, 17);
            this.lblNome.TabIndex = 11;
            this.lblNome.Text = "Nome:";
            // 
            // lblVice
            // 
            this.lblVice.AutoSize = true;
            this.lblVice.Font = new System.Drawing.Font("Consolas", 10F);
            this.lblVice.Location = new System.Drawing.Point(9, 149);
            this.lblVice.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblVice.Name = "lblVice";
            this.lblVice.Size = new System.Drawing.Size(128, 17);
            this.lblVice.TabIndex = 13;
            this.lblVice.Text = "Vice candidato:";
            // 
            // txtCandidato
            // 
            this.txtCandidato.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCandidato.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCandidato.Location = new System.Drawing.Point(142, 31);
            this.txtCandidato.Name = "txtCandidato";
            this.txtCandidato.Size = new System.Drawing.Size(279, 23);
            this.txtCandidato.TabIndex = 19;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.cboPartido);
            this.panel3.Controls.Add(this.btnCadastrar);
            this.panel3.Controls.Add(this.nudNumero);
            this.panel3.Controls.Add(this.imgCandidato);
            this.panel3.Controls.Add(this.button1);
            this.panel3.Controls.Add(this.txtVice);
            this.panel3.Controls.Add(this.lblVice);
            this.panel3.Controls.Add(this.lblNumero);
            this.panel3.Controls.Add(this.lblPartido);
            this.panel3.Controls.Add(this.txtCandidato);
            this.panel3.Controls.Add(this.lblNome);
            this.panel3.Location = new System.Drawing.Point(12, 84);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(631, 246);
            this.panel3.TabIndex = 22;
            // 
            // imgCandidato
            // 
            this.imgCandidato.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.imgCandidato.Location = new System.Drawing.Point(444, 31);
            this.imgCandidato.Name = "imgCandidato";
            this.imgCandidato.Size = new System.Drawing.Size(169, 133);
            this.imgCandidato.TabIndex = 23;
            this.imgCandidato.TabStop = false;
            // 
            // button1
            // 
            this.button1.BackgroundImage = global::Nsf.ProjetoUrna.APP.ELEITOR.Properties.Resources.magnifier_glass_icon_icons_com_71148;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button1.Location = new System.Drawing.Point(571, 165);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(42, 22);
            this.button1.TabIndex = 24;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtVice
            // 
            this.txtVice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtVice.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVice.Location = new System.Drawing.Point(142, 143);
            this.txtVice.Name = "txtVice";
            this.txtVice.Size = new System.Drawing.Size(279, 23);
            this.txtVice.TabIndex = 22;
            // 
            // nudNumero
            // 
            this.nudNumero.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudNumero.Location = new System.Drawing.Point(142, 69);
            this.nudNumero.Name = "nudNumero";
            this.nudNumero.Size = new System.Drawing.Size(279, 23);
            this.nudNumero.TabIndex = 25;
            this.nudNumero.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // btnCadastrar
            // 
            this.btnCadastrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCadastrar.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCadastrar.Location = new System.Drawing.Point(528, 202);
            this.btnCadastrar.Name = "btnCadastrar";
            this.btnCadastrar.Size = new System.Drawing.Size(85, 24);
            this.btnCadastrar.TabIndex = 26;
            this.btnCadastrar.Text = "Cadastrar";
            this.btnCadastrar.UseVisualStyleBackColor = true;
            this.btnCadastrar.Click += new System.EventHandler(this.btnCadastrar_Click);
            // 
            // cboPartido
            // 
            this.cboPartido.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPartido.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.cboPartido.FormattingEnabled = true;
            this.cboPartido.Location = new System.Drawing.Point(142, 105);
            this.cboPartido.Name = "cboPartido";
            this.cboPartido.Size = new System.Drawing.Size(279, 25);
            this.cboPartido.TabIndex = 27;
            // 
            // frmCadastro_Candidatos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(655, 333);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmCadastro_Candidatos";
            this.Text = "frmCadastrodeCandidatos";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgCandidato)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudNumero)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblNumero;
        private System.Windows.Forms.Label lblPartido;
        private System.Windows.Forms.Label lblNome;
        private System.Windows.Forms.Label lblVice;
        private System.Windows.Forms.TextBox txtCandidato;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.PictureBox imgCandidato;
        private System.Windows.Forms.TextBox txtVice;
        private System.Windows.Forms.NumericUpDown nudNumero;
        private System.Windows.Forms.Button btnCadastrar;
        private System.Windows.Forms.ComboBox cboPartido;
    }
}