﻿using Nsf.ProjetoUrna.Business;
using Nsf.ProjetoUrna.Business.Urna;
using Nsf.ProjetoUrna.FERRAMENTAS.Plugin;
using Nsf.ProjetoUrna.MODELO.Candidato;
using Nsf.ProjetoUrna.MODELO.Urna;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf.ProjetoUrna.APP.ELEITOR
{
    public partial class frmCadastro_Candidatos : Form
    {
        public frmCadastro_Candidatos()
        {
            InitializeComponent();
            CarregarCombo();
        }

        private void CarregarCombo ()
        {
            Business_Partido db = new Business_Partido();
            List<DTO_Partido> list = db.Listar();

            cboPartido.ValueMember = nameof(DTO_Partido.ID);
            cboPartido.DisplayMember = nameof(DTO_Partido.Partido);

            cboPartido.DataSource = list;
        }

        private void btnCadastrar_Click(object sender, EventArgs e)
        {
            DTO_Partido partido = cboPartido.SelectedItem as DTO_Partido;
            DTO_Candidato dto = new DTO_Candidato();

            dto.ID_Partido = partido.ID;
            dto.Candidato = txtCandidato.Text;
            dto.Numero = Convert.ToInt32(nudNumero.Value);
            dto.Vice = txtVice.Text;
            dto.Imagem = ImagemPlugin.ConverterParaString(imgCandidato.Image);

            Business_Candidato db = new Business_Candidato();
            db.Salvar(dto);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Instâncio a função que irá abrir os documentos Windows/Desktop.
            OpenFileDialog dialog = new OpenFileDialog();

            //Faz um filtro apenas em JPAG ou PNG.
            dialog.Filter = @"JPG Files(*.jpg)|*.jpg|PNG Files(*.png)|*.png";

            //Habilita a caixa de arquivos Windows.
            DialogResult result = dialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                //Pega a localização da imagem.
                imgCandidato.ImageLocation = dialog.FileName;
            }
        }

    }
}
