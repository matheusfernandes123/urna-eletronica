﻿using Nsf.ProjetoUrna.Business.Eleitor;
using Nsf.ProjetoUrna.MODELO.Eleitor;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf._2018.ProjetoUrna.Urna.Eleitor
{
    public partial class frmRegistrar : Form
    {
        public frmRegistrar()
        {
            InitializeComponent();

            //Deixa para próxima ( ͡° ͜ʖ ͡°)

            //List<string> turma = new List<string>();
            //turma.Add("INFORMÁTICA-A");
            //turma.Add("INFORMÁTICA-B");
            //turma.Add("INFORMÁTICA-C");
            //turma.Add("INFORMÁTICA-D");
            //
            //cboTurma.DataSource = turma;
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        public void Cadastrar(int id, string nome, string cpf)
        {
            try
            {
                DTO_Eleitor dto = new DTO_Eleitor();
                dto.ID = id;
                dto.Eleitor = nome;
                dto.CPF = cpf;

                Business_Eleitor db = new Business_Eleitor();
                db.Registrar(dto);

                lblMensagem.BackColor = Color.Maroon;
                lblMensagem.ForeColor = Color.White;
                lblMensagem.Text = "Mesário cadastrado com sucesso!!";
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message,
                                "Info-B, AVISO!!",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Stop);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro fatal. Tente mais tarde!",
                                "Info-B, AVISO!!",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Stop);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int id = int.Parse(lblID.Text);
            string nome = txtNome.Text;
            string cpf = txtCPF.Text;

            Cadastrar(id, nome, cpf);
        }

        private void txtNome_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                int id = int.Parse(lblID.Text);
                string nome = txtNome.Text;
                string cpf = txtCPF.Text;

                Cadastrar(id, nome, cpf);
            }
        }
    }
}
