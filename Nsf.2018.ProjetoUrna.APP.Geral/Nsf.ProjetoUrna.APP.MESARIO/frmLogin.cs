﻿using Nsf.ProjetoUrna.Business.Mesário;
using Nsf.ProjetoUrna.MODELO.Mesário;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf._2018.ProjetoUrna.Urna.Mesário
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
        }

        public void Acesso(string nome, string senha)
        {
            try
            {
                DTO_Mesario dto = new DTO_Mesario();
                dto.Mesario = nome;
                dto.Senha = senha;

                Business_Mesario db = new Business_Mesario();
                DTO_Mesario user = db.Logar(dto);

                if (user != null)
                {
                    this.Hide();
                    frmMesario tela = new frmMesario();
                    tela.Show();
                }
                else
                {
                    MessageBox.Show("Credênciais inválidas" + "\n\n" + "Favor falar com ADM",
                                    "Info-B, AVISO!!",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Stop);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro fatal: " + "\n\n" + ex.Message,
                                "Info-B, AVISO!!",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Question);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string nome = txtNome.Text;
            string senha = txtSenha.Text;

            Acesso(nome, senha);
        }

        private void txtNome_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                string nome = txtNome.Text;
                string senha = txtSenha.Text;

                Acesso(nome, senha);
            }
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void label4_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmRegistro tela = new frmRegistro();
            tela.Show();
        }
    }
}
