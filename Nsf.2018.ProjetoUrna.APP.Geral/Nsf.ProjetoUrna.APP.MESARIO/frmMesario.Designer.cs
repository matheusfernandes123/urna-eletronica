﻿namespace Nsf._2018.ProjetoUrna.Urna.Mesário
{
    partial class frmMesario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvMesario = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CPF = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Permissão = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pnlMesario = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.lblFechar = new System.Windows.Forms.Label();
            this.lblBusca = new System.Windows.Forms.Label();
            this.txtEleitor = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMesario)).BeginInit();
            this.panel1.SuspendLayout();
            this.pnlMesario.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvMesario
            // 
            this.dgvMesario.AllowUserToAddRows = false;
            this.dgvMesario.AllowUserToDeleteRows = false;
            this.dgvMesario.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.dgvMesario.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvMesario.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvMesario.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvMesario.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMesario.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.CPF,
            this.Permissão});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvMesario.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgvMesario.GridColor = System.Drawing.SystemColors.ActiveBorder;
            this.dgvMesario.Location = new System.Drawing.Point(26, 169);
            this.dgvMesario.Name = "dgvMesario";
            this.dgvMesario.ReadOnly = true;
            this.dgvMesario.RowHeadersVisible = false;
            this.dgvMesario.Size = new System.Drawing.Size(474, 306);
            this.dgvMesario.TabIndex = 13;
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column1.DataPropertyName = "Eleitor";
            this.Column1.FillWeight = 192.126F;
            this.Column1.HeaderText = "Nome do Eleitor";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // CPF
            // 
            this.CPF.DataPropertyName = "CPF";
            this.CPF.FillWeight = 7.874008F;
            this.CPF.HeaderText = "CPF";
            this.CPF.Name = "CPF";
            this.CPF.ReadOnly = true;
            this.CPF.Width = 150;
            // 
            // Permissão
            // 
            this.Permissão.DataPropertyName = "Permissao";
            this.Permissão.HeaderText = "Permissão";
            this.Permissão.Name = "Permissão";
            this.Permissão.ReadOnly = true;
            this.Permissão.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Permissão.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(3)))), ((int)(((byte)(70)))), ((int)(((byte)(178)))));
            this.panel1.Controls.Add(this.pnlMesario);
            this.panel1.Controls.Add(this.lblFechar);
            this.panel1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(527, 59);
            this.panel1.TabIndex = 16;
            // 
            // pnlMesario
            // 
            this.pnlMesario.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(21)))), ((int)(((byte)(1)))));
            this.pnlMesario.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlMesario.Controls.Add(this.label1);
            this.pnlMesario.Location = new System.Drawing.Point(6, 4);
            this.pnlMesario.Name = "pnlMesario";
            this.pnlMesario.Size = new System.Drawing.Size(131, 51);
            this.pnlMesario.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(0, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(118, 32);
            this.label1.TabIndex = 7;
            this.label1.Text = "Mesário";
            // 
            // lblFechar
            // 
            this.lblFechar.AutoSize = true;
            this.lblFechar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblFechar.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFechar.ForeColor = System.Drawing.Color.White;
            this.lblFechar.Location = new System.Drawing.Point(498, 16);
            this.lblFechar.Name = "lblFechar";
            this.lblFechar.Size = new System.Drawing.Size(26, 27);
            this.lblFechar.TabIndex = 8;
            this.lblFechar.Text = "X";
            // 
            // lblBusca
            // 
            this.lblBusca.AutoSize = true;
            this.lblBusca.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(21)))), ((int)(((byte)(1)))));
            this.lblBusca.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBusca.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblBusca.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Bold);
            this.lblBusca.ForeColor = System.Drawing.Color.White;
            this.lblBusca.Location = new System.Drawing.Point(66, 91);
            this.lblBusca.Name = "lblBusca";
            this.lblBusca.Size = new System.Drawing.Size(78, 25);
            this.lblBusca.TabIndex = 15;
            this.lblBusca.Text = "Buscar:";
            // 
            // txtEleitor
            // 
            this.txtEleitor.BackColor = System.Drawing.Color.Snow;
            this.txtEleitor.Font = new System.Drawing.Font("Arial Narrow", 11F);
            this.txtEleitor.Location = new System.Drawing.Point(65, 117);
            this.txtEleitor.MaxLength = 50;
            this.txtEleitor.Name = "txtEleitor";
            this.txtEleitor.Size = new System.Drawing.Size(384, 24);
            this.txtEleitor.TabIndex = 14;
            // 
            // frmMesario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(527, 490);
            this.Controls.Add(this.dgvMesario);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lblBusca);
            this.Controls.Add(this.txtEleitor);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmMesario";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmMesario";
            ((System.ComponentModel.ISupportInitialize)(this.dgvMesario)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.pnlMesario.ResumeLayout(false);
            this.pnlMesario.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvMesario;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn CPF;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Permissão;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel pnlMesario;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblFechar;
        private System.Windows.Forms.Label lblBusca;
        private System.Windows.Forms.TextBox txtEleitor;
    }
}