﻿using Nsf.ProjetoUrna.DATABASE.Eleitor;
using Nsf.ProjetoUrna.MODELO.Eleitor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf.ProjetoUrna.Business.Eleitor
{
    public class Business_Eleitor
    {
        Database_Eleitor db = new Database_Eleitor();

        public int Registrar(DTO_Eleitor dto)
        {
            if (dto.CPF.Trim() == string.Empty)
            {
                throw new ArgumentException("CPF inválido!!");
            }
            if (dto.Eleitor.Trim() == string.Empty)
            {
                throw new ArgumentException("Nome do eleitor inválido!!");
            }
            bool checar = db.VerificarCPF(dto);
            if (checar == true)
            {
                throw new ArgumentException("CPF já consta em nossa base de dados!");
            }
            int id = db.Salvar(dto);
            return id;
        }

        public DTO_Eleitor Logar(DTO_Eleitor dto)
        {
            if (dto.CPF.Trim() == string.Empty)
            {
                throw new ArgumentException("CPF inválido!!");
            }
            if (dto.Eleitor.Trim() == string.Empty)
            {
                throw new ArgumentException("Nome do eleitor inválido!!");
            }
            dto = db.Logar(dto);
            return dto;
        }

        public List<DTO_Eleitor> Consultar(DTO_Eleitor dto)
        {
            List<DTO_Eleitor> consult = db.Consultar(dto);
            return consult;
        }

        public void Remover(DTO_Eleitor dto)
        {
            db.Remover(dto);
        }

        public void Alterar(DTO_Eleitor dto)
        {
            db.Alterar(dto);
        }
    }
}
