﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf.ProjetoUrna.APP.ELEITOR
{
    public class Business_Permissao
    {
        public bool ConsultarPermissao(DTO_Permissao dto)
        {
            Database_Permissao db = new Database_Permissao();
            return db.ConsultarPermissao(dto);
        }
        public void Alterar(int id, bool dto)
        {
            Database_Permissao db = new Database_Permissao();
            db.Alterar(id, dto);
        }
    }
}
