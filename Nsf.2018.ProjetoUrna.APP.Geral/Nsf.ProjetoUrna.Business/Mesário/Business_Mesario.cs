﻿using Nsf.ProjetoUrna.DATABASE.Mesário;
using Nsf.ProjetoUrna.MODELO.Mesário;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf.ProjetoUrna.Business.Mesário
{
    public class Business_Mesario
    {
        Database_Mesario db = new Database_Mesario();

        public DTO_Mesario Logar(DTO_Mesario dto)
        {
            return db.Logar(dto);
        }

        public int Salvar(DTO_Mesario dto)
        {
            if (dto.Mesario.Trim() == string.Empty)
            {
                throw new ArgumentException("Nome Inválido");
            }
            if (dto.Senha.Trim() == string.Empty)
            {
                throw new ArgumentException("Senha Inválida");
            }

            return db.Salvar(dto);
        }
    }
}
