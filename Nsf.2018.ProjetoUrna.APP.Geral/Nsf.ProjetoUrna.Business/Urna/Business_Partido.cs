﻿using Nsf.ProjetoUrna.DATABASE.Urna;
using Nsf.ProjetoUrna.MODELO.Urna;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf.ProjetoUrna.Business.Urna
{
    public class Business_Partido
    {
        public List<DTO_Partido> Listar()
        {
            Database_Partido db = new Database_Partido();
            List<DTO_Partido> listar = db.Listar();

            return listar;
        }
    }
}
