﻿using MySql.Data.MySqlClient;
using Nsf.ProjetoUrna.MODELO.Eleitor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf.ProjetoUrna.DATABASE.Eleitor
{
    public class Database_Eleitor
    {
        Base.Database db = new Base.Database();

        public int Salvar(DTO_Eleitor dto)
        {
            string script =
               @"INSERT INTO tb_eleitor
                 (
                    nm_eleitor, 
                    ds_cpf,
                    bt_permissao
                 )
                 VALUES
                (   @nm_eleitor, 
                    @ds_cpf, 
                    @bt_permissao
                )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_eleitor", dto.Eleitor));
            parms.Add(new MySqlParameter("ds_cpf", dto.CPF));
            parms.Add(new MySqlParameter("bt_permissao", dto.Permissao));

            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public DTO_Eleitor Logar(DTO_Eleitor dto)
        {
            string script = @"SELECT * FROM tb_eleitor 
                                       WHERE nm_eleitor = @nm_eleitor 
                                       AND ds_cpf = @ds_cpf";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_eleitor", dto.Eleitor));
            parms.Add(new MySqlParameter("ds_cpf", dto.CPF));

            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            //Null
            DTO_Eleitor fora = null;

            if (reader.Read())
            {
                //Instancia normal
                fora = new DTO_Eleitor();
                fora.ID = reader.GetInt32("id_eleitor");
                fora.Eleitor = reader.GetString("nm_eleitor");
                fora.CPF = reader.GetString("ds_cpf");
                fora.Permissao = reader.GetBoolean("bt_permissao");
            }
            reader.Close();
            return fora;
        }

        public bool VerificarCPF(DTO_Eleitor dto)
        {
            string script = @"SELECT * FROM tb_eleitor 
                                      WHERE ds_cpf = @ds_cpf";
            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("ds_cpf", dto.CPF));
            MySqlDataReader reader = db.ExecuteSelectScript(script, parm);

            if (reader.Read())
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        public List<DTO_Eleitor> Consultar(DTO_Eleitor dto)
        {
            string script = @"SELECT * FROM tb_eleitor
                                      WHERE nm_eleitor LIKE  @nm_eleitor";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("nm_eleitor", "%" + dto.Eleitor + "%"));

            MySqlDataReader reader = db.ExecuteSelectScript(script, parm);
            List<DTO_Eleitor> fora = new List<DTO_Eleitor>();

            while (reader.Read())
            {
                DTO_Eleitor dt = new DTO_Eleitor();
                dt.ID = reader.GetInt32("id_eleitor");
                dt.Eleitor = reader.GetString("nm_eleitor");
                dt.CPF = reader.GetString("ds_cpf");
                dt.Permissao = reader.GetBoolean("bt_permissao");

                fora.Add(dt);
            }
            reader.Close();
            return fora;
        }

        public void Remover(DTO_Eleitor dto)
        {
            string script = @"DELETE FROM tb_eletiro
                                    WHERE id_eleitor = @id_eleitor";
            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("id_eleitor", dto.ID));

            db.ExecuteInsertScript(script, parm);
        }

        public void Alterar(DTO_Eleitor dto)
        {
            string script = @"UPDATE tb_eleitor 
                                 SET nm_eleitor          =@nm_eleitor,
                                     ds_cpf              =@ds_cpf,
                                     bt_permissao        =@bt_permissao
                              WHERE  id_eleitor          =@id_eleitor";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_eleitor", dto.Eleitor));
            parms.Add(new MySqlParameter("ds_cpf", dto.CPF));
            parms.Add(new MySqlParameter("bt_permissao", dto.Permissao));
            parms.Add(new MySqlParameter("id_eleitor", dto.ID));

            db.ExecuteInsertScript(script, parms);
        }
    }
}
