﻿using MySql.Data.MySqlClient;
using Nsf.ProjetoUrna.DATABASE.Base;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf.ProjetoUrna.APP.ELEITOR
{
    public class Database_Permissao
    {
        public bool ConsultarPermissao(DTO_Permissao dto)
        {
            string script = @"SELECT * FROM tb_permissao 
                                      WHERE id_permissao = @id_permissao";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_permissao", dto.ID));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<DTO_Permissao> lista = new List<DTO_Permissao>();

            while (reader.Read())
            {
                DTO_Permissao dt = new DTO_Permissao();
                //É necessário ler a referência da tabela
                dto.ID = reader.GetInt32("id_permissao");
                dto.Situacao = reader.GetBoolean("ds_situacao");
                lista.Add(dto);
            }
            reader.Close();

            return dto.Situacao;
        }
        public void Alterar (int id, bool dto)
        {
            string script =
                @"UPDATE tb_permissao
                     SET ds_situacao  = @ds_situacao
                   WHERE id_permissao = @id_permissao";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("id_permissao", id));
            parm.Add(new MySqlParameter("ds_situacao", dto));

            Database db = new Database();
            db.ExecuteInsertScript(script, parm);
        }
    }
}
