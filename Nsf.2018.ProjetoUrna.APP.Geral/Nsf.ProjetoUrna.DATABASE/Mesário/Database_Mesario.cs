﻿using MySql.Data.MySqlClient;
using Nsf.ProjetoUrna.DATABASE.Base;
using Nsf.ProjetoUrna.MODELO.Mesário;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf.ProjetoUrna.DATABASE.Mesário
{
    public class Database_Mesario
    {
        Database db = new Database();

        public int Salvar(DTO_Mesario dto)
        {
            string script =
            @"INSERT INTO tb_mesario
            (
                nm_mesario,
                ds_senha
            )
            VALUES
            (
              @nm_mesario,
              @ds_senha
            )";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("nm_mesario", dto.Mesario));
            parm.Add(new MySqlParameter("ds_senha", dto.Senha));

            return db.ExecuteInsertScriptWithPk(script, parm);
        }

        public DTO_Mesario Logar(DTO_Mesario dto)
        {
            string script = @"SELECT * FROM tb_mesario
                                      WHERE nm_mesario =  @nm_mesario
                                        AND ds_senha   =  @ds_senha";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("nm_mesario", dto.Mesario));
            parm.Add(new MySqlParameter("ds_senha", dto.Senha));


            MySqlDataReader reader = db.ExecuteSelectScript(script, parm);
            DTO_Mesario fora = null;

            if (reader.Read())
            {
                fora = new DTO_Mesario();
                fora.ID = reader.GetInt32("id_mesario");
                fora.Mesario = reader.GetString("nm_mesario");
                fora.Senha = reader.GetString("ds_senha");
            }
            reader.Close();
            return fora;
        }
    }
}
