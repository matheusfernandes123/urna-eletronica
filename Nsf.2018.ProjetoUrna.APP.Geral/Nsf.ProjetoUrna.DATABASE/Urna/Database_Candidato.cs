﻿using MySql.Data.MySqlClient;
using Nsf.ProjetoUrna.DATABASE.Base;
using Nsf.ProjetoUrna.MODELO;
using Nsf.ProjetoUrna.MODELO.Candidato;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf.ProjetoUrna.DATABASE.Candidato
{
    public class Database_Candidato
    {
        public int Salvar (DTO_Candidato dto)
        {
            string script =
            @"INSERT INTO tb_candidato
            (   
                id_partido,
                nm_candidato,
                nr_candidato,
                nm_vice,
                img_foto
            )
            VALUES
            (
                @id_partido,
                @nm_candidato,
                @nr_candidato,
                @nm_vice,
                @img_foto
            )";
            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("id_partido", dto.ID_Partido));
            parm.Add(new MySqlParameter("nm_candidato", dto.Candidato));
            parm.Add(new MySqlParameter("nr_candidato", dto.Numero));
            parm.Add(new MySqlParameter("nm_vice", dto.Vice));
            parm.Add(new MySqlParameter("img_foto", dto.Imagem));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parm);
        }
    }
}
