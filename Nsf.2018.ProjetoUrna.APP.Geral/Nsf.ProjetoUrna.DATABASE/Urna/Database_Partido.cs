﻿using MySql.Data.MySqlClient;
using Nsf.ProjetoUrna.DATABASE.Base;
using Nsf.ProjetoUrna.MODELO.Urna;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf.ProjetoUrna.DATABASE.Urna
{
    public class Database_Partido
    {
        public List<DTO_Partido> Listar ()
        {
            string script = @"SELECT * FROM tb_partido";

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, null);
            List<DTO_Partido> fora = new List<DTO_Partido>();

            while (reader.Read())
            {
                DTO_Partido dentro = new DTO_Partido();
                dentro.ID = reader.GetInt32("id_partido");
                dentro.Partido = reader.GetString("nm_partido");

                fora.Add(dentro);
            }
            reader.Close();
            return fora;
        }
    }
}
