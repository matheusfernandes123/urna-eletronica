﻿using MySql.Data.MySqlClient;
using Nsf.ProjetoUrna.DATABASE.Base;
using Nsf.ProjetoUrna.MODELO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf.ProjetoUrna.DATABASE
{
    public class VIEW_CandidatoDatabase
    {
        public List<VIEW_CandidatoDTO> Listar()
        {
            string script = @"SELECT * FROM view_consultar_candidato";
            List<MySqlParameter> parm = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parm);
            List<VIEW_CandidatoDTO> lista = new List<VIEW_CandidatoDTO>();
            while (reader.Read())
            {
                VIEW_CandidatoDTO dt = new VIEW_CandidatoDTO();
                dt.ID = reader.GetInt32("id_candidato");
                dt.Candidato = reader.GetString("nm_candidato");
                dt.Partido = reader.GetString("nm_partido");
                dt.Numero = reader.GetInt32("nr_candidato");
                dt.Suplente = reader.GetString("nm_vice");
                dt.Foto = reader.GetString("img_foto");
                lista.Add(dt);
            }

            reader.Close();
            return lista;
        }
    }
}
