﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf.ProjetoUrna.MODELO.Eleitor
{
    public class DTO_Eleitor
    {
        public int ID { get; set; }
        public string Eleitor { get; set; }
        public string CPF { get; set; }
        public bool Permissao { get; set; }
    }
}
