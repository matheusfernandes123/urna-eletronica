﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf.ProjetoUrna.MODELO.Mesário
{
    public class DTO_Mesario
    {
        public int ID { get; set; }
        public string Mesario { get; set; }
        public string Senha { get; set; }
        public bool Permissao { get; set; }
    }
}
