﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf.ProjetoUrna.MODELO.Candidato
{
    public class DTO_Candidato
    {
        public int ID { get; set; }
        public int ID_Partido { get; set; }
        public string Candidato { get; set; }
        public int Numero { get; set; }
        public string Vice { get; set; }
        public string Imagem { get; set; }

    }
}
