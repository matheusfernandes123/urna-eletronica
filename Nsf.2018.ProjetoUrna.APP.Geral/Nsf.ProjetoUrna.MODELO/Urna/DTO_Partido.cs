﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf.ProjetoUrna.MODELO.Urna
{
    public class DTO_Partido
    {
        public int ID { get; set; }
        public string Partido { get; set; }
    }
}
