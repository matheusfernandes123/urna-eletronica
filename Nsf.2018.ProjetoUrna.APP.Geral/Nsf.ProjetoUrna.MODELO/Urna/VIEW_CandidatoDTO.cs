﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf.ProjetoUrna.MODELO
{
    public class VIEW_CandidatoDTO
    {
        public int ID { get; set; }
        public string Candidato { get; set; }
        public string Partido { get; set; }
        public int Numero { get; set; }
        public string Suplente { get; set; }
        public string Foto { get; set; }
    }
}
